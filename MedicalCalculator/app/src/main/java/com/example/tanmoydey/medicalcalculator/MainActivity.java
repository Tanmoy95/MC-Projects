package com.example.tanmoydey.medicalcalculator;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
public class MainActivity extends AppCompatActivity {
    double weightEntered;
    double convertionRate = 2.2;
    double convertedWeight;
    EditText editText;
    TextView textView;
    RadioGroup radioGroup;
    RadioButton radioButton1;
    RadioButton radioButton2;

    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.weightValue);
        radioGroup = findViewById(R.id.radioGroup);
        radioButton1 = findViewById(R.id.poundToKg);
        radioButton2 = findViewById(R.id.kgToPound);
        textView = findViewById(R.id.textView2);
    }



    public void radioBtnClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.poundToKg:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Pound to Kilogram Checked!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.kgToPound:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Kilogram to Pound Checked!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void convert(View view) {

        weightEntered = Double.parseDouble(editText.getText().toString());
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        if (radioButton1.isChecked()) {
            if (weightEntered <= 500) {
                convertedWeight = weightEntered / convertionRate;
                textView.setText(decimalFormat.format(convertedWeight) + " Kilogram");
            } else {
                Toast.makeText(MainActivity.this, "Pounds must be less than 500", Toast.LENGTH_SHORT).show();
            }
        }

        if (radioButton2.isChecked()) {
            if (weightEntered <= 225) {
                convertedWeight = weightEntered * convertionRate;
                textView.setText(decimalFormat.format(convertedWeight) + " Pounds");
            } else {
                Toast.makeText(MainActivity.this, "Kilogram must be less than 225", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
